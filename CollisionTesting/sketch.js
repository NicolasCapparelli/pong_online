var mainRect;
var mainCircle;

// Main loop for p5js basically, runs once
function setup() {
    // Creates the canvas we're going to draw in
    createCanvas(800, 500);

    // Creating the paddle
    mainRect = new Rectangle();

    // Creating the ball
    mainCircle = new Circle();


}

// Also required method for p5js. Runs 60 times per second
function draw() {
    background(51);

    handleMovement();
    mainCircle.collides(mainRect);

    mainCircle.render();
    mainCircle.updateDetectionPoint(mainRect);
    mainCircle.renderDetectionPoint();

    mainRect.render();
    mainRect.updateVertices();
    line(mainCircle.posx, mainCircle.posy, mainRect.center[0], mainRect.center[1])

}

function handleMovement() {

    // Circle movement
    if(keyIsDown(38)){ // Up arrow
        mainCircle.posy -= 2;
    }

    else if(keyIsDown(40)) { // Down arrow
        mainCircle.posy += 2;
    }

    else if(keyIsDown(39)) { // Right arrow
        mainCircle.posx += 2;
    }

    else if(keyIsDown(37)) { // Right arrow
        mainCircle.posx -= 2;
    }

    // Rectangle Movement
    if(keyIsDown(87) ){
        mainRect.posy -= 5;
        // mainRect.updateVertices();
    }

    else if(keyIsDown(83)) {
        mainRect.posy += 5;
        // mainRect.updateVertices();
    }

}