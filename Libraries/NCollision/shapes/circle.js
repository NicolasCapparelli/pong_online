class Circle{

    constructor() {
        // Attribute
        this.posx = 200;
        this.posy = 200;
        this.width = 20;

        // Radius of the ball (Determines roundness)
        this.radius = 10;
        this.detectionPoint = [];

    }

    render() {

        fill(255, 204, 0);
        // Draws ball (Ellipse)
        ellipse(this.posx, this.posy, this.radius * 2, this.radius * 2)
    }

    updateDetectionPoint(obj){
        this.angle = Math.atan2(obj.center[1] - this.posy, obj.center[0] - this.posx) * (180 / Math.PI);
        this.detectionPoint = [this.posx + this.radius * Math.cos(this.angle * (Math.PI / 180)), this.posy + this.radius * Math.sin(this.angle * (Math.PI / 180))]
    }

    renderDetectionPoint(){

        fill(255, 204, 0);
        // Draws ball (Ellipse)
        ellipse(this.detectionPoint[0], this.detectionPoint[1], 3, 3)
    }


    // Takes in rectangle object
    collides(obj){

        // IF the detection point X is less than the object's B vertex's X (The detection point is past the front of the rectangle)
        if(this.detectionPoint[0] < obj.vertB[0]){

            // IF the detection point Y is greater than the B vertex's Y AND less than the C vertex's Y (In between the two vertices)
            if (this.detectionPoint[1] > obj.vertB[1] && this.detectionPoint[1] < obj.vertC[1]){

                // Register hit
                console.log("HIT")
            }

        }
    }

}