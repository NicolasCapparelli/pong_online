class Rectangle{

    // Initiates the values for the paddle
    constructor(){

        // Attributes
        this.shape = "rect";
        this.posx = 100;
        this.posy = 100;
        this.width = 70;
        this.height = 100;
        this.updateVertices()
    }

    render(){

        fill(255, 204, 0);
        // Draws rectangle that represents the paddle
        rect(this.posx, this.posy, this.width, this.height);

        // Draws circles on the vertices DEBUG
        ellipse(this.vertA[0], this.vertA[1], 5 , 5);
        ellipse(this.vertB[0], this.vertB[1], 5 , 5);
        ellipse(this.vertC[0], this.vertC[1], 5 , 5);
        ellipse(this.vertD[0], this.vertD[1], 5 , 5);

        fill(255, 0, 0);
        ellipse(this.center[0], this.center[1], 5 , 5);


    }

    updateVertices(){

        // Vertices
        this.vertA = [this.posx, this.posy];
        this.vertB = [this.posx + this.width, this.posy];
        this.vertC = [this.posx + this.width, this.posy + this.height];
        this.vertD = [this.posx, this.posy + this.height];

        // Center
        this.center = [this.posx + this.width/2, this.posy + this.height/2];

    }
}
