// @Param circle: Ncollision Circle or child class of that
// @Param rectangle: Ncollision Rectangle or child class of that
// @Param direction: Where the rectangle is relative to the circle (False = Left of the circle | True = Right of circle)
function nCollisionCircleRectangle(circle, rectangle, direction) {


    // IF the rectangle is to the left of the circle
    if (!direction) {
        // IF the detection point X is less than the object's B vertex's X (The detection point is past the front of the rectangle)
        if (circle.detectionPoint[0] < rectangle.vertB[0] && circle.detectionPoint[0] > rectangle.vertA[0]) {

            // IF the detection point Y is greater than the B vertex's Y AND less than the C vertex's Y (In between the two vertices)
            return circle.detectionPoint[1] > rectangle.vertB[1] && circle.detectionPoint[1] < rectangle.vertD[1];
        }
    }

    else {

        // IF the detection point X is greater than the object's A vertex's X (The detection point is past the front of the rectangle)
        if (circle.detectionPoint[0] > rectangle.vertA[0] && circle.detectionPoint[0] < rectangle.vertB[0]) {

            // IF the detection point Y is greater than the A vertex's Y AND less than the  vertex's Y (In between the two vertices)
            return circle.detectionPoint[1] > rectangle.vertB[1] && circle.detectionPoint[1] < rectangle.vertC[1];
        }
    }
}


// @Param circleOne: Ncollision Circle or child class of that
// @Param circleTwo: Ncollision Circle or child class of that
function nCollisionCircleCircle(circleOne, circleTwo) {

    // Return true if the distance between the two circles is LESS THAN the radius of the first circle plus the radius of the second circle
    // Otherwise it will return false
    return dist(circleOne.posx, circleOne.posy, circleTwo.posx, circleTwo.posy) < circleOne.radius + circleTwo.radius;
}