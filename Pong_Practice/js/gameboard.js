class GameBoard{
    constructor(width, height){
        this.bWidth = width;
        this.bHeight = height;
        this.playerOneScore = 0;
        this.playerTwoScore = 0;
        this.bgColor = 52;
        this.powerUpInField = false;
        this.debug = false;
        this.gameStart = false;

    }


    // Updates the score given which side the ball was scored on (false == left | true == right)
    updateScore(direction){

        if (direction){
            this.playerOneScore += 1;
        }

        else {
            this.playerTwoScore += 1;
        }

    }
}