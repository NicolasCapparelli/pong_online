const paddleWidth = 15;
const paddleHeight = 100;

class Paddle{

    // @Param initX: x position of where the paddle will be initially drawn
    // @Param initY: y position of where the paddle will be initially drawn
    constructor(initX, initY){
        this.shape = "rect";
        this.posx = initX;
        this.posy = initY;
        this.velx = 5;
        this.vely = 5;
        this.width = paddleWidth;
        this.height = paddleHeight;

        this.updateVertices()
    }

    render(){
        // Draws rectangle that represents the paddle
        rect(this.posx, this.posy, this.width, this.height)
    }

    updateVertices(){

        // Vertices
        this.vertA = [this.posx, this.posy];
        this.vertB = [this.posx + this.width, this.posy];
        this.vertC = [this.posx + this.width, this.posy + this.height];
        this.vertD = [this.posx, this.posy + this.height];

        // Center
        this.center = [this.posx + this.width/2, this.posy + this.height/2];

    }

    renderVertices(){

        // Draws circles on the vertices DEBUG
        ellipse(this.vertA[0], this.vertA[1], 5 , 5);
        ellipse(this.vertB[0], this.vertB[1], 5 , 5);
        ellipse(this.vertC[0], this.vertC[1], 5 , 5);
        ellipse(this.vertD[0], this.vertD[1], 5 , 5);

        fill(255, 0, 0);
        ellipse(this.center[0], this.center[1], 5 , 5);
    }
}

Paddle.getSize = function () {
    return [paddleWidth, paddleHeight]
};
