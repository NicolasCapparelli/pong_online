const effects = ["fastball", "slowpaddle", "reverse", "crazy_background", "small_paddle", "big_paddle"];

class PowerUp{

    constructor(x, y, objDictionary){
        this.descriptor = "powerup";
        this.posx = x;
        this.posy = y;
        this.radius = 4;
        this.objects = objDictionary;
        this.playerToEffect = 2;

        // Determines whether or not to render the powerup on the game field
        this.visible = true;

        // Amount of time in frames that the powerup lasts for. (Game runs at 60 FPS so 60 frames == 1 second)
        this.cooldown = 600;

        // The number of frames the frameCount is at when the a player hits the powerup and the powerup is deployed
        this.deployed = 0;

        // Randomly picks an effect
        this.effect = effects[Math.floor(random(effects.length))];
    }

    render(){
        fill(0, 255, 0);
        ellipse(this.posx, this.posy, this.radius * 2, this.radius * 2)
    }

    detectCollision(){

        let ball = this.objects["ball"];

        if (nCollisionCircleCircle(this, ball)){

            // If the ball is traveling to the right (direction == true), have the powerup affect the left paddle
            if (ball.direction){
                this.playerToEffect = 0;
            }

            // If the ball is traveling to the left (direction == false), have the powerup affect the right paddle
            else {
                this.playerToEffect = 1;
            }

            // Set the time the powerup was deployed to the current number of frames
            this.deployed = frameCount;

            // Hide the powerup in the field
            this.visible = false;


            this.effectObjects(false);
        }
    }

    // Causes the actual effects of the powerup to happen | Takes in boolean reverse which if true will reverse the effect
    effectObjects(reverse){

        let modifier = 0;

        switch (this.effect) {

            case "fastball":

                modifier = 3;

                if (reverse){
                    this.objects["ball"].velx -= modifier;
                }

                else {
                    this.objects["ball"].velx += modifier;
                }

                break;

            case "slowpaddle":

                modifier = 3;

                if (reverse){
                    this.objects["paddles"][this.playerToEffect].vely +=  modifier;
                }

                else {
                    this.objects["paddles"][this.playerToEffect].vely -= modifier;
                }

                break;

            case "reverse":

                this.objects["ball"].direction = !this.objects["ball"].direction;

                break;

            case "crazy_background":

                if (reverse) {
                    this.objects["gameboard"].bgColor = 52
                }

                else {
                    let colors = ["red", "blue", "green"];
                    this.objects["gameboard"].bgColor = colors[Math.floor(random(colors.length))];
                }

                break;

            case "small_paddle":

                modifier = 20;

                if (reverse){
                    this.objects["paddles"][this.playerToEffect].height +=  modifier;
                }

                else {
                    this.objects["paddles"][this.playerToEffect].height -=  modifier;
                }

                break;

            case "big_paddle":

                modifier = 20;

                if (reverse){
                    this.objects["paddles"][this.playerToEffect].height -= modifier;
                }

                else {
                    this.objects["paddles"][this.playerToEffect].height += modifier;
                }

                break;
        }
    }

    checkCooldown(){

        // If the frameCount when the powerup was deployed (this.deployed) PLUS the cooldown is less than the current frameCount
        if (this.deployed + this.cooldown < frameCount){

            console.log("COOLDOWN OVER");

            // Reverse the effect of the powerup
            this.effectObjects(true);

            // Tell the gameboard there is no longer a powerup on the field
            this.objects["gameboard"].powerUpInField = false;

            // Completely remove the powerup
            delete this.objects["powerup"];

        }

    }

}