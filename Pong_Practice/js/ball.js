// A "child" class if you will of an Ncollision Circle class
class Ball{
    constructor() {

        // Attributes
        this.descriptor = "ball";
        this.posx = 200;
        this.posy = 200;
        this.velx = 3;
        this.vely = 2;
        this.width = 20;

        // A point on the edge of a circle to test versus rectangle like objects for collision. Holds X [0] and Y [1]
        this.detectionPoint = [];

        // Radius of the ball (Determines roundness)
        this.radius = 10;

        // Direction the ball should move. false == left | true == right
        this.direction = true;

        // Determines whether the ball is going up or down. false == down | true == up
        this.gravitySide = false;
    }

    // Renders ball object
    render(){
        // Draws ball (Ellipse)
        ellipse(this.posx, this.posy, this.radius * 2, this.radius * 2)
    }

    // Updates the point (x,y) that is checked versus the object that its going to collide with
    // @Param objArray: Array of objects to check against
    updateDetectionPoint(objArray){

        // The paddle that will be checked
        let paddleToTest;

        // If the ball is traveling to the right, check the right paddle
        if(this.direction){
            paddleToTest = objArray[1];
        }

        // Else traveling to the left, check the left paddle
        else {
            paddleToTest = objArray[0];
        }

        this.angle = Math.atan2(paddleToTest.center[1] - this.posy, paddleToTest.center[0] - this.posx) * (180 / Math.PI);
        this.detectionPoint = [this.posx + this.radius * Math.cos(this.angle * (Math.PI / 180)), this.posy + this.radius * Math.sin(this.angle * (Math.PI / 180))]
    }

    renderDetectionPoint(){

        fill(255, 204, 0);
        // Draws ball (Ellipse)
        ellipse(this.detectionPoint[0], this.detectionPoint[1], 3, 3)
    }


    // Takes in paddle object
    checkCollisionPaddle(paddleArray){

        // The paddle that will be checked
        let paddleToTest;

        // If the ball is traveling to the right, check the right paddle
        if(this.direction){
            paddleToTest = paddleArray[1];
        }

        // Else traveling to the left, check the left paddle
        else {
            paddleToTest = paddleArray[0];
        }

        // Call collision detection
        if (nCollisionCircleRectangle(this, paddleToTest, this.direction)){
            this.direction = !this.direction;
        }
    }

    reset(){
        this.posx = 200;
        this.posy = 200;
    }
}
