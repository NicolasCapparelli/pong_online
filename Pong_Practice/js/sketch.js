// https://p5js.org/reference/#/p5/redraw

//
let objectsInField = {};
let gameBoard = new GameBoard(800, 500);
const xPowerUpBounds = {lower: 50, upper: 500};
const yPowerUpBounds = {lower:30, upper: 470};

// Main loop for p5js basically, runs once
function setup() {

    // The height at which the paddles will spawn
    const paddleSpawnHeight = height/2;

    // Creates the canvas we're going to draw in
    createCanvas(gameBoard.bWidth, gameBoard.bHeight);

    // Adding the gameboard to objects array to be able to affect it later with powerup
    objectsInField["gameboard"] = gameBoard;

    // Creating the ball
    objectsInField["ball"] = new Ball();

    // Create two paddle objects and put them in an array, then in the key of objectsInFiled "paddles"
    objectsInField["paddles"] = [new Paddle(0,paddleSpawnHeight), new Paddle(width - Paddle.getSize()[0] - 1, paddleSpawnHeight)];

    // Event listener for P keypress that will start the game
    window.addEventListener("keypress", function (e) {

        if (e.which === 80){
            console.log("Starting Game");
            gameBoard.gameStart = true;
        }
    }, false);
}

// Also required method for p5js. Runs 60 times per second
function draw() {

    // Set the color of the background
    background(gameBoard.bgColor);

    // Allow movement
    handlePaddleMovement();
    handleBallMovement();

    // Ball handling, heh
    objectsInField["ball"].render();
    objectsInField["ball"].updateDetectionPoint(objectsInField["paddles"]);
    objectsInField["ball"].renderDetectionPoint();
    objectsInField["ball"].checkCollisionPaddle(objectsInField["paddles"]);

    // Paddle One handling
    objectsInField["paddles"][0].render();
    objectsInField["paddles"][0].updateVertices();

    //Paddle Two handling
    objectsInField["paddles"][1].render();
    objectsInField["paddles"][1].updateVertices();


    // If 30 seconds has passed calculated with frame rate and there are no other powerups on the field
    // (Framerate = 60 frames per second, 60 x 30 = 1800 check if the amount of frames that have passed is evenly divisible by 1800)
    if (!gameBoard.powerUpInField && frameCount % 60 === 0) {

        // Create a new powerup object in the field with a random X and Y coordinate, within the bounds
        objectsInField["powerups"] = new PowerUp(random(xPowerUpBounds["lower"], xPowerUpBounds["upper"]), random(yPowerUpBounds["lower"], yPowerUpBounds["upper"]), objectsInField)
        gameBoard.powerUpInField = true;
    }

    if (gameBoard.powerUpInField){

        if (objectsInField["powerups"].visible){
            objectsInField["powerups"].render();
            objectsInField["powerups"].detectCollision();
        }

        if (objectsInField["powerups"].deployed !== 0){
            objectsInField["powerups"].checkCooldown();
        }
    }

    // Drawing the score
    textSize(32);
    text(gameBoard.playerOneScore.toString(), width/2 - 20, 40);
    text(gameBoard.playerTwoScore.toString(), width/2 + 20, 40);

}

function handlePaddleMovement() {

    let leftPaddle = objectsInField["paddles"][0];
    let rightPaddle = objectsInField["paddles"][1];

    // Left paddle movement
    if(keyIsDown(87) && leftPaddle.posy > 0){

        leftPaddle.posy -= leftPaddle.vely;
    }

    else if(keyIsDown(83) && leftPaddle.posy < (500 - leftPaddle.height)) {
        leftPaddle.posy += leftPaddle.vely;
    }

    // Right paddle movement
    if(keyIsDown(38) && rightPaddle.posy > 0){

        rightPaddle.posy -= rightPaddle.vely;
    }

    else if(keyIsDown(40) && rightPaddle.posy < (500 - rightPaddle.height)) {
        rightPaddle.posy += rightPaddle.vely;
    }

}

function debug() {


}

function handleBallMovement() {

    let ball = objectsInField["ball"];

    if (!gameBoard.debug && gameBoard.gameStart) {

        // Horizontal movement
        if (ball.direction) {
            ball.posx += ball.velx;
        }

        else {
            ball.posx -= ball.velx;

        }

        // Vertical Movement
        if (ball.gravitySide) {
            ball.posy -= ball.vely;
        }

        else {
            ball.posy += ball.vely;
        }

        // Vertical bound detection
        if (ball.posy + ball.radius >= height || ball.posy - ball.radius <= 0) {
            ball.gravitySide = !ball.gravitySide;
        }

        // Horizontal Bound Detection
        if (ball.posx + ball.radius > width){
            ball.reset();
            gameBoard.updateScore(true);
        }

        else if (ball.posx - ball.radius < 0){
            ball.reset();
            gameBoard.updateScore(false)
        }
    }

    else if (gameBoard.debug){
        ball.posx = mouseX;
        ball.posy = mouseY;

        if (keyIsDown(191)){
            gameBoard.debug = false;
        }
    }

    else if (!gameBoard.gameStart && !gameBoard.debug){

    }
}

