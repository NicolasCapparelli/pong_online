// Make socket connection with server
var socket = io.connect("http://localhost:4000/", {'connect timeout': 1000});

// Get elements from DOM (Document Object Model aka the document lol)
var divOutput = document.getElementById('output');
var inHandle = document.getElementById('handle');
var inMessage = document.getElementById('message');
var btnSend = document.getElementById('send');

// Emit (Send) Events through the socket connection to the server
btnSend.addEventListener('click', sendMessage);

// Listen for data sent from the server through the socket connection
socket.on('chat', onChatMessageReceived);

// FUNCTIONS-SEND //

// What happens when the user hits the send button
function sendMessage() {

    // Data to be sent to server
    var data = {
        user: inHandle.value,
        message: inMessage.value
    };

    // Sending data to server with message name "chat"
    socket.emit('chat', data);
}


// FUNCTIONS-RECEIVE //

function onChatMessageReceived(data) {

    // Adding to the innerHTML of the output div
    divOutput.innerHTML += '<p><strong>' + data.user + ":</strong> " + data.message + "</p>"
}
