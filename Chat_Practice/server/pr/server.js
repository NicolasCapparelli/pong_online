// Requires (Dependencies)
var express = require('express');
var socket = require('socket.io');

// Instantiating Express
var app = express();

// Telling the server to listen on port 4000 of this IP (localhost)
var server = app.listen(4000, onServerConnect);

// Serving the static files in the public and public/html folder to whoever connects (Static files are ones that aren't dynamic
app.use(express.static('public'));
app.use(express.static('public/html'));


// Instantiating Socket.io with the server
var io = socket(server);

// Tell the server to run the onSocketEstablished function when a socket connection is made
// This will pass a socket argument to that function which is the socket that the specific client and the server have open
io.on('connection', onSocketEstablished);

// What happens when a computer connects with the server (via request)
function onServerConnect() {
    // Logging action
    console.log("listening on 4000....")
}

// What happens when a socket connection is established
function onSocketEstablished(socket) {

    console.log("SOCKET ESTABLISHED WITH: " + socket.id);

    // Socket message received listeners (Tells the server what to do when certain messages are received through this socket)
    // @param 0: The message name
    // @param 1: The function to execute when received
    socket.on('chat', onChatMessageReceived);

    // What happens when a chat message is received
    function onChatMessageReceived(data) {

        console.log("MESSAGE RECEIVED FROM: " + socket.id);

        // Sending the data received from one client to ALL clients (io.sockets refers to all sockets connected)
        io.emit('chat', data);
    }
}
