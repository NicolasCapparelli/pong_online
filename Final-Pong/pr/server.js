// Requires (Dependencies)
var express = require('express');
var socket = require('socket.io');
const GameRoom = require('./gameroom.js');

// Instantiating Express
var app = express();

// Telling the server to listen on port 4000 of this IP (localhost)
var server = app.listen(4000, onServerConnect);

// Serving the static files in the public and public/html folder to whoever connects (Static files are ones that aren't dynamic
app.use(express.static('public'));
app.use(express.static('public/html'));


// Instantiating Socket.io with the server
var io = socket(server);

// Tell the server to run the onSocketEstablished function when a socket connection is made
// This will pass a socket argument to that function which is the socket that the specific client and the server have open
io.on('connection', onSocketEstablished);

// What happens when a computer connects with the server (via request)
function onServerConnect() {
    // Logging action
    console.log("listening on 4000....")
}

// Setting up gamerooms
const MAX_PLAYERS = 2;
const maxRooms = 3;
let gameRooms = {};


for (let i = 0; i < maxRooms; i++) {
    let serverName = "Server " + i.toString();
    gameRooms[serverName] = new GameRoom(serverName, MAX_PLAYERS, io);
}


// What happens when a socket connection is established
function onSocketEstablished(socket) {

    console.log("SERVER>>> SOCKET ESTABLISHED WITH: " + socket.id);

    // Whether this specific socket is in a gameroom
    let isInGameRoom = false;

    // The name of the gameroom that this socket is in
    let gameRoomIn = "";

// LISTENERS //

    // Socket message received listeners (Tells the server what to do when certain messages are received through this socket)
    // @param 0: The message name
    // @param 1: The function to execute when received
    socket.on('disconnect', onSocketDisconnected);
    socket.on('serverList', onServerListRequested);
    socket.on('reservationRequest', onReservationRequested);
    socket.on('chatMessage', onChatMessageReceived);


// FUNCTIONS //

    // What happens when a chat message is received
    function onServerListRequested() {

        // Dictionary to send to client that contains gamerooms
        let gameRoomList = {};

        // Creating the objects to add to the new dictionary above from the local gameroom dictionary
        for (let key in gameRooms) {
            gameRoomList[key] = {
                roomName: gameRooms[key].roomName,
                players: gameRooms[key].players,
                maxPlayers: gameRooms[key].maxPlayers,
                availability: gameRooms[key].availability

            };
        }

        // The final object to send to the client
        let serverList = {
            servers: gameRoomList
        };

        // Send data back to client
        socket.emit('serverList', serverList);
    }

    // What happens when the client requests a reservation for a server
    function onReservationRequested(data) {

        let serverName = data.serverRequested;
        let reservationRequestResponse;

        // If the gameroom is not full of players and the socketID of the client isn't already in a server
        if (gameRooms[serverName].players !== MAX_PLAYERS && gameRooms[serverName].playerIDs.indexOf(socket.id) === -1){

            // Add one to the amount of players in the room
            gameRooms[serverName].players += 1;

            // Add the socket ID of the client to the gameroom
            gameRooms[serverName].playerIDs.push(socket.id);

            // Add this socket to the socket room of the gameroom for easy communication
            socket.join(gameRooms[serverName].socketRoom);

            // Adjusting this socket's properties that I created
            isInGameRoom = true;
            gameRoomIn = serverName;

            // Response
            reservationRequestResponse = {
                response: "joined"
            }

        }

        // If the server the client is trying to join is full
        else if (gameRooms[serverName].players === MAX_PLAYERS){
            reservationRequestResponse = {
                response: "full"
            }
        }

        // If the client is already in the server they requested to join (SHOULD NOT HAPPEN)
        else if (gameRooms[serverName].playerIDs.indexOf(socket.id) > -1){
            reservationRequestResponse = {
                response: "alreadyin"
            }
        }

        // If something I don't know about happens lol
        else {
            reservationRequestResponse = {
                response: "error"
            }
        }

        // Send a response to the client telling it if it was able to join the gameroom or not
        socket.emit('reservationRequestResponse', reservationRequestResponse);

    }

    // When a chat message is received from the client
    function onChatMessageReceived(data) {

        console.log("CHAT MESSAGE RECEIVED BB");
        
        // Send the message to everyone in the gameroom
        io.to(gameRooms[gameRoomIn].roomName).emit('chatMessage', data);

        console.log(io.sockets.adapter.rooms);


        // TODO: ROOMS NOT WORKING FIX PLS
    }

    // When the socket connection has been lost
    function onSocketDisconnected() {

        // If the socket is in a gameroom, remove it's ID from that gameroom and decrement the player count by one
        if (isInGameRoom){
            gameRooms[gameRoomIn].players -= 1;
            gameRooms[gameRoomIn].playerIDs.splice(gameRooms[gameRoomIn].playerIDs.indexOf(socket.id));
        }

    }
}