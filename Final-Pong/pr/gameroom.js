class GameRoom {

    // Takes in maximum number of players for the room as roomSize
    constructor(roomName, roomSize, socketServer){

        this.roomName = roomName;
        this.maxPlayers = roomSize;
        this.players = 0;
        this.availability = false;

        // Socket IDs of the players in the room
        this.playerIDs = [];
        this.socketRoom = socketServer.of('/' + roomName);
    }
}

module.exports = GameRoom;


