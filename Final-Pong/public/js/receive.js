// Listen for data sent from the server through the socket connection
window.SOCKET.on('serverList', onAvailServersReceived);
window.SOCKET.on('reservationRequestResponse', onReservationRequestResponse);
window.SOCKET.on('chatMessage', onChatMessageReceived);

//// FUNCTIONS - MENU PAGE ////
function onAvailServersReceived(data) {

    // Remove loading screen
    $('#serverLoading').css('display', 'none');

    let serverMenuContainer = document.getElementById('serverTableBody');
    console.log();
    let finalData = data.servers;

    // Hide the floating loading screen
    $('#floatingLoadingScreen').css('visibility', 'hidden');

    // Create new entries in the serverTable with the data received from the server
    for (let key in finalData) {
        if (finalData.hasOwnProperty(key)) {
            serverMenuContainer.innerHTML += '<tr tabindex="-1" class="serverRow"><td class="serverTableC1">' + finalData[key].roomName + '</td><td class="serverTableC2">' + finalData[key].players + '/' + finalData[key].maxPlayers + '</td><td class="serverTableC3">' + finalData[key].availability + '</td></tr>';
        }
    }

// TODO: Figure out why the visibility: hidden doesn't hide the loading screen

}

function onReservationRequestResponse(data) {

    // The text inside the loading screen to be changed in case of an issue
    let loadingText = $('#floatingLoadingScreenCaption');

    switch (data.response) {

        case "joined":

            $('#floatingLoadingScreen').css('visibility', 'hidden');
            shrinkMenu(true);
            break;

        case "alreadyin":

            // Not really sure what to do here because this shouldn't happen but...1
            console.log("The client is already in this gameroom, should not happen");
            break;

        case "full":

            // TODO: Test this

            // Change the text of the loading screen
            loadingText.text("The server you are trying to join is already full... Redirecting to menu");

            // Wait 3 seconds and refresh the page
            setTimeout(function () {

                // Refresh the page
                location.reload();

                // TODO: At some point in time, have the menu come flying back in instead of just refreshing page you lazy fuck.
            }, 3000);

            break;

        case "error":

            // Change loading text
            loadingText.text("There was an unexpected error... Refreshing page...");

            // Wait 3 seconds and refresh the page
            setTimeout(function () {
                location.reload();
            }, 3000);
            break;
    }
}

//// FUNCTIONS - GAME PAGE ////

// When a chat messaged is received from the server via socket connection
function onChatMessageReceived(data) {

    console.log("CHAT RECEIVED");

    // Append the message to the message board
    $('#messageBoard').prepend('<p class="chatMessage"><strong>' + data.username + ": " + '</strong>' + data.message +  '</p>');

    // TODO: Test this
}
