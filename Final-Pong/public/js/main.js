jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
        $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
        $(window).scrollLeft()) + "px");
    return this;
};

// Data about the player
var CLIENT_PLAYER = {

    username: '',
    paddleSkin: '',
    ballSkin: ''

};

// Make socket connection with server
var SOCKET = io.connect("http://localhost:4000/", {'connect timeout': 1000});

// Grabbing the buttons and setting them to vars
let btnPlay = document.getElementById('btnPlay');
let btnServerSearch = document.getElementById('btnServerSearch');
let btnSettings = document.getElementById('btnSettings');
let btnSaveSettings = document.getElementById('btnSaveSettings');

//// LISTENERS ////

// When the user hits play
btnPlay.addEventListener('click', function () {
    animateMenuOut();

});

// When the server search button is pressed
btnServerSearch.addEventListener('click', function () {
    growMenu();
    $('#floatingLoadingScreen').css("visibility", "visible");
    SOCKET.emit('serverList');
});

btnSettings.addEventListener('click', function () {
    growSettingsMenu();
});

btnSaveSettings.addEventListener('click', function () {
    onSettingsSaved();
});


//// FUNCTIONS ////

// Removes the menu in an animated fashion
function animateMenuOut() {

    // Have all the buttons be animated away
    $('#gameTitle').animate({top: '15px'}, function () {
        $('#gameTitle').animate({top: '-400px'}, function () {
            $('#btnPlay').animate({bottom: '100%'}, function () {
                $('#btnServerSearch').animate({bottom: '100%'}, function () {
                    $('#btnSettings').animate({bottom: '120%'}, doneAnimation);
                });
            });
        });
    });

    // When the animation is done, hide the gameMenu completely
    function doneAnimation() {
        $('#gameMenu').css("visibility", "hidden");
    }
}

// Grows server search menu in an animated fashion
function growMenu() {

    // Remove the text of the server search button
    $('#btnServerSearch').html('');

    // Make the menu's backdrop as big the page
    $('#ssmBackdrop').css('visibility', 'visible')
        .css('height', '100%')
        .css('width', '100%')

        // Add click event listener but ignore child clicks
        .click(function (){
          shrinkMenu(false);  
        }).children().click(function () {
        return false;
    });

    // Grow the server search menu
    $('#serverSearchMenu').css('visibility', "visible")
        .animate({height: '80%'}, 'slow', function () {
            $('#serverSearchMenu').animate({width: '80%'}, 'slow');
            $('#tableWrapper').css("visibility", "visible");
        });
}


// Shrinks server search menu in an animated fashion
function shrinkMenu(playClicked = false) {


    $('#tableWrapper').css("visibility", "hidden");

    // Shrink the server search menu
    $('#serverSearchMenu').animate({height: '10px'}, 'slow', function () {

        // TODO: Figure out delay glitch when closing the second time around, fix on the settings menu shrink too
        $('#serverSearchMenu').animate({width: '10px'}, 'slow', function () {

            // Hide the menu
            $('#floatingLoadingScreen').css("visibility", "hidden");
            $('#serverSearchMenu').css('visibility', "hidden");

            // Remove the current servers that were added when the server search was opened
            $('#serverTableBody').empty();

            // Add the server search button text back
            $('#btnServerSearch').html('Server Search');

            // Remove visibility of backdrop
            $('#ssmBackdrop').css('visibility', "hidden");

            // If there is a function that needs to wait for this one to finish
            if (playClicked) {
                animateMenuOut();
            }

        });
    });
}

// Settings
function growSettingsMenu(){

    // Remove the text of the server search button
    $('#btnSettings').html('');

    // Make the menu's backdrop as big the page
    $('#settingsBackdrop').css('visibility', 'visible')
        .css('height', '100%')
        .css('width', '100%')

        // Add click event listener but ignore child clicks
        .click(shrinkSettingsMenu).children().click(function () {
        return false;
    });

    // Grow the server search menu
    $('#settingsMenu').css('visibility', "visible")
        .animate({height: '90%'}, 'slow', function () {
            $('#settingsMenu').animate({width: '50%'}, 'slow');
            $('#settingsValueWrapper').css("visibility", "visible");
        });

}

function shrinkSettingsMenu() {

    $('#settingsValueWrapper').css("visibility", "hidden");

    // Shrink the server search menu
    $('#settingsMenu').animate({height: '10px'}, 'slow', function () {

        $('#settingsMenu').animate({width: '10px'}, 'slow', function () {

            $('#settingsMenu').css('visibility', "hidden");

            // Add the server search button text back
            $('#btnSettings').html('Settings');

            // Remove visibility of backdrop
            $('#settingsBackdrop').css('visibility', "hidden");

        });
    });
}

function onSettingsSaved (){

    // Setting the value of the CLIENT_PLAYER to the new ones in settings
    CLIENT_PLAYER.username = $('#usernameInput').val();
    CLIENT_PLAYER.paddleSkin = $('#paddleSkinInput').val();
    CLIENT_PLAYER.ballSkin = $('#ballSkinInput').val();

    // Closing the settings menu
    shrinkSettingsMenu();

}