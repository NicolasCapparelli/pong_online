// Used to holder the selected server on the server search menu
var serverSelected = null;


//// MENU PAGE ////

// Used for selecting which server to chose
$('#serverList').click(function () {
    if (document.activeElement.getAttribute("class") === "serverRow") {
        serverSelected = $(':focus');
    }

});

// When the GO button is pressed in the serverSelectMenu
$('#btnSelectServer').click(function () {

    // Get the first child's value (Server Name) of the row selected
    let serverName = serverSelected.children(":first").html();

    // Send to the server
    var data = {
        serverRequested: serverName
    };

    // Send a reservation request to the server
    window.SOCKET.emit('reservationRequest', data);

    // Show the floatingLoadingScreen and change the text
    $('#floatingLoadingScreen').css('visibility', 'visible');
    $('#floatingLoadingScreenCaption').text('Establishing Connection...');

});

//// GAME PAGE ////

$('#sendMessage').click(function () {

    // Message the user writes
    let message = $('#messageInput').val();

    // Data to send to server
    let data = {
        user: window.CLIENT_PLAYER.username,
        chatMessage: message
    };

    // Send message to server
    window.SOCKET.emit('chatMessage', data);



});

$('#messageInput').keypress(function (e) {

    let message = $('#messageInput').val();

    if (e.keyCode === 13) {
        // Append the message to the message board
        $('#messageBoard').prepend('<p class="chatMessage"><strong>' + CLIENT_PLAYER.username + ": " + '</strong>' + message +  '</p>')
    }
});

// TODO: Merge the above two functions and actually implement with socket, k?